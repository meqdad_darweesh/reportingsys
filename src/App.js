import React, { Component } from "react";
import "./App.css";
import { Route } from "react-router-dom";
import HomePage from "./components/pages/homePage";
import LoginPage from "./components/pages/loginPage";
import Dashboard from "./components/dashboard";

/*
The predefined credentials:
username: 123
password: 321
*/

class App extends Component {
  render() {
    return (
      <div className="App">
        <Route path="/" exact component={HomePage} />
        <Route path="/loginPage" exact component={LoginPage} />
        <Route path="/dashboard" exact component={Dashboard} />
      </div>
    );
  }
}

export default App;
