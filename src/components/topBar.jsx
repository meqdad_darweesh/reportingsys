import React, { Component } from "react";
import { Input } from "antd";

const Search = Input.Search;

class TopBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search_value: ""
    };
  }

  searching = e => {
    this.setState({ search_value: e.target.value });
    this.props.searching(this.state.search_value);
    console.log("term in TopBar: ", this.state.search_value);
  };

  render() {
    return (
      <React.Fragment>
        <Search
          placeholder="Search..."
          size="large"
          onChange={this.searching.bind(this)}
          enterButton
        />
      </React.Fragment>
    );
  }
}

export default TopBar;
