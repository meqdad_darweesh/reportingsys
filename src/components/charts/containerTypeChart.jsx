import React, { Component } from "react";
import { BarChart } from "react-d3-components";

class ContainerTypeChart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: this.props.containerTypeData,
      length: this.props.containerTypeData.length
    };
  }

  render() {
    let info = this.state.data;
    let containerTypeInfo = [];

    let k,
      z,
      w,
      ctr = 0; //counters...

    // Loop, for extracting the container_type field from the all sent data.
    for (k = 0; k < this.state.length; k++) {
      containerTypeInfo[k] = info[k].container_type;
    }

    // Finding the unique values from the container_type field.
    let uniqueContainerTypes = [
      ...new Set(containerTypeInfo) /*.add("Jawwal")*/
    ];
    /*
var data = [{
    label: 'somethingA',
    values: [{x: 'SomethingA', y: 10}, {x: 'SomethingB', y: 4}, {x: 'SomethingC', y: 3}]
}];
*/
    let shapedData = [
      {
        label: "Container Types",
        values: [{ x: "", y: 0 }, { x: "", y: 0 }]
      }
    ];

    // Filling and formulating the data.
    for (z = 0; z < uniqueContainerTypes.length; z++) {
      shapedData[0].values[z].x = uniqueContainerTypes[z]; // Label filling.

      w = 0;
      ctr = 0;

      for (w = 0; w < this.state.length; w++) {
        if (uniqueContainerTypes[z] === containerTypeInfo[w]) {
          ++ctr;
          shapedData[0].values[z].y = ctr;
        }
      }
    }

    return (
      <BarChart
        data={shapedData}
        width={400}
        height={400}
        margin={{ top: 10, bottom: 50, left: 50, right: 10 }}
      />
    );
  }
}

export default ContainerTypeChart;
