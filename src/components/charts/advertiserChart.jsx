import React, { Component } from "react";
import { PieChart } from "react-d3-components";

// let PieChartSahpe = ReactD3.PieChart;
let sort = null; // d3.ascending, d3.descending, func(a,b) { return a - b; }, etc...

class AdvertiserNameChart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: this.props.advertisersData,
      length: this.props.advertisersData.length
    };
  }
  render() {
    let info = this.state.data;
    let advertiserInfo = [];
    console.log("data in chart" + JSON.stringify(this.state.data));
    console.log("data length in chart" + JSON.stringify(this.state.length));

    let k, z, w;
    for (k = 0; k < this.state.length; k++) {
      advertiserInfo[k] = info[k].advertiser_name;
    }

    let uniqueAdvertisers = [...new Set(advertiserInfo) /*.add("Jawwal")*/];
    console.log("uniqueAdvertisers: ", uniqueAdvertisers);

    let shapedData = [{ x: "", y: this.state.length }, { x: "", y: 0 }];

    // A loop for filling X in the PieChart values.
    for (z = 0; z < uniqueAdvertisers.length; z++) {
      // formulate()
      shapedData[z].x = uniqueAdvertisers[z];
      console.log("shapedData[z].x: ", shapedData[z].x);
    }

    let ctr = 0;
    for (k = 0; k < uniqueAdvertisers.length; k++) {
      ctr = 0;
      for (w = 0; w < this.state.length; w++) {
        if (uniqueAdvertisers[k] === advertiserInfo[w]) {
          ++ctr;
          shapedData[k].y = ctr;
        }
      }
    }

    let data = {
      label: "Advertiser Name",
      values: shapedData
      // values: [
      //   { x: "SomethingA", y: 10 },
      //   { x: "SomethingB", y: 40 },
      //   { x: "SomethingC", y: 3 }
      // ]
    };

    return (
      <PieChart
        data={data}
        width={500}
        height={400}
        margin={{ top: 10, bottom: 10, left: 50, right: 50 }}
        sort={sort}
      />
    );
  }
}

export default AdvertiserNameChart;
