import React, { Component } from "react";
import DataTable from "./dataTable";
import { Layout, Spin } from "antd";
import { Button } from "antd/lib/radio";

const { Header, Footer, /*Sider,*/ Content } = Layout;

const url = "http://adcast.ps:8002/logs/api/v1/campaign-test/";

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // searchQue: "",
      loading: true,
      fetched: null
    };
  }

  async componentDidMount() {
    // Fetching data from API
    const response = await fetch(url);
    const data = await response.json();
    this.setState({ loading: false, fetched: data });
  }

  logout = () => {
    this.setState({ fetched: null, loading: false });
    this.props.history.push("/");
  };

  render() {
    if (this.state.loading) {
      return (
        <div style={{ margin: 200 }}>
          <Spin tip="Loading..." size="large" />
        </div>
      );
    }

    if (!this.state.fetched) {
      return <div>Didn't get data from API.</div>;
    }

    return (
      <div>
        <Layout>
          <Layout>
            <Header>
              <Button onClick={this.logout}>Logout</Button>
            </Header>
            <Content>
              <DataTable jsonData={this.state.fetched} />
            </Content>
            <Footer />
          </Layout>
        </Layout>
      </div>
    );
  }
}

/*
<Sider>
  <TableMenu />
</Sider>
*/
export default Dashboard;

/*
  // Data example...
  const rows = [
    {
      id: 62336546,
      date: "2018-09-19",
      time: "23:48:00",
      device_id: 14,
      campaign_name: "shabab",
      container_type: "Popup",
      created_server: "2018-09-20 00:40:39.630174+03",
      advertiser: "Paltel"
    },
    {
      id: 62336551,
      date: "2018-09-19",
      time: "23:48:10",
      device_id: 14,
      campaign_name: "shabab",
      container_type: "Popup",
      created_server: "2018-09-20 00:40:39.653642+03",
      advertiser: "Paltel"
    },

    {
      id: 62336557,
      date: "2018-09-19",
      time: "23:48:20",
      device_id: 14,
      campaign_name: "shabab",
      container_type: "Popup",
      created_server: "2018-09-20 00:40:39.665573+03",
      advertiser: "Paltel"
    },

    {
      id: 62336562,
      date: "2018-09-19",
      time: "23:48:30",
      device_id: 14,
      campaign_name: "shabab",
      container_type: "Popup",
      created_server: "2018-09-20 00:40:39.677513+03",
      advertiser: "Paltel"
    },
    {
      id: 62336582,
      date: "2018-09-22",
      time: "23:49:20",
      device_id: 14,
      campaign_name: "test12",
      container_type: "Popup",
      created_server: "2018-09-20 00:40:39.747155+03",
      advertiser: "Jawwal"
    },
    {
      id: 62336587,
      date: "2018-09-29",
      time: "23:49:30",
      device_id: 14,
      campaign_name: "test12",
      container_type: "Popup",
      created_server: "2018-09-20 00:40:39.749097+03",
      advertiser: "Jawwal"
    },
    {
      id: 62336608,
      date: "2018-09-13",
      time: "23:50:50",
      device_id: 14,
      campaign_name: "shabab-4",
      container_type: "Banner",
      created_server: "2018-09-20 00:40:39.801622+03",
      advertiser: "Arab Bank"
    },
    {
      id: 62181496,
      date: "2018-10-11",
      time: "16:20:50",
      device_id: 14,
      campaign_name: "shabab-4",
      container_type: "Banner",
      created_server: "2018-09-18 17:00:43.536349+03",
      advertiser: "Arab Bank"
    },
    {
      id: 62181497,
      date: "2018-10-18",
      time: "16:23:20",
      device_id: 14,
      campaign_name: "shabab",
      container_type: "Banner",
      created_server: "2018-09-18 17:00:43.548309+03",
      advertiser: "Arab Bank"
    },
    {
      id: 62181501,
      date: "2018-11-18",
      time: "16:24:00",
      device_id: 14,
      campaign_name: "shabab",
      container_type: "Popup",
      created_server: "2018-09-18 17:00:43.559183+03",
      advertiser: "Arab Bank"
    },
    {
      id: 62336557,
      date: "2018-11-19",
      time: "23:48:20",
      device_id: 14,
      campaign_name: "shabab",
      container_type: "Popup",
      created_server: "2018-09-20 00:40:39.665573+03",
      advertiser: "Paltel"
    },

    {
      id: 62336562,
      date: "2018-09-19",
      time: "23:48:30",
      device_id: 14,
      campaign_name: "shabab",
      container_type: "Popup",
      created_server: "2018-09-20 00:40:39.677513+03",
      advertiser: "Paltel"
    },
    {
      id: 62336582,
      date: "2018-09-19",
      time: "23:49:20",
      device_id: 14,
      campaign_name: "test12",
      container_type: "Popup",
      created_server: "2018-09-20 00:40:39.747155+03",
      advertiser: "Jawwal"
    },
    {
      id: 62336587,
      date: "2018-09-19",
      time: "23:49:30",
      device_id: 14,
      campaign_name: "test12",
      container_type: "Popup",
      created_server: "2018-09-20 00:40:39.749097+03",
      advertiser: "Jawwal"
    },
    {
      id: 62336608,
      date: "2018-10-22",
      time: "23:50:50",
      device_id: 14,
      campaign_name: "shabab-4",
      container_type: "Banner",
      created_server: "2018-09-20 00:40:39.801622+03",
      advertiser: "Arab Bank"
    },
    {
      id: 62181496,
      date: "2018-11-10",
      time: "16:20:50",
      device_id: 14,
      campaign_name: "shabab-4",
      container_type: "Banner",
      created_server: "2018-09-18 17:00:43.536349+03",
      advertiser: "Arab Bank"
    },
    {
      id: 62181497,
      date: "2018-11-27",
      time: "16:23:20",
      device_id: 14,
      campaign_name: "shabab",
      container_type: "Banner",
      created_server: "2018-09-18 17:00:43.548309+03",
      advertiser: "Arab Bank"
    },
    {
      id: 62181501,
      date: "2018-09-18",
      time: "16:24:00",
      device_id: 14,
      campaign_name: "shabab",
      container_type: "Popup",
      created_server: "2018-09-18 17:00:43.559183+03",
      advertiser: "Arab Bank"
    }
  ];
  */
