import React, { Component } from "react";
import { Input, Button } from "antd";

class SearchFilter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchText: ""
    };
  }

  handleSearch = () => {
    this.props.onSearch(this.state.searchText);
  };

  handleReset = () => {
    this.setState({ searchText: "" });
  };

  render() {
    return (
      <div className="custom-filter-dropdown">
        <Input
          ref={ele => (this.searchInput = ele)}
          placeholder="Search..."
          value={this.state.searchText}
          onChange={e =>
            e.target.value ? this.setState({ searchText: e.target.value }) : []
          }
          onPressEnter={this.handleSearch.bind(this)}
        />
        <Button type="primary" onClick={this.handleSearch.bind(this)}>
          Search
        </Button>
        <Button onClick={this.handleReset.bind(this)}>Reset</Button>
      </div>
    );
  }
}

export default SearchFilter;
