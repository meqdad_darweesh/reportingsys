import React, { Component } from "react";
import Summary from "./summary";
import { Table, Input, Button, Icon } from "antd";
import { DatePicker, TimePicker } from "antd";
import moment from "moment";
import AdvertiserNameChart from "./charts/advertiserChart";
import ContainerTypeChart from "./charts/containerTypeChart";

const { RangePicker } = DatePicker;

let startDate = new Date(),
  endDate = new Date(),
  lastData = [];

class DataTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: this.props.jsonData,
      no_of_items: this.props.jsonData.length,
      searchText: "",
      start_time_value: undefined,
      end_time_value: undefined,
      date_filter: false,
      time_filter: false,
      page_number: 1
    };
  }

  handleSearch = (selectedKeys, confirm) => () => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  };

  handleReset = clearFilters => () => {
    clearFilters();
    this.setState({ searchText: "" });
  };

  onChange = (pagination, filters, sorter) => {
    console.log("params", pagination, filters, sorter);
    this.setState({ page_number: pagination.current });
  };

  onChangeDate = (date /*, dateString*/) => {
    this.setState({ date_filter: true });
    console.log("date: ", date[0], " .. ", date[1]);

    let formattedStartDate = moment(date[0], "YYYY-MM-DD");
    let formattedEndDate = moment(date[1], "YYYY-MM-DD");

    startDate = formattedStartDate.toDate();
    endDate = formattedEndDate.toDate();

    if (startDate && endDate) {
    }

    console.log("startDate: ", JSON.stringify(startDate));
    console.log("endDate: ", JSON.stringify(endDate));
  };

  onChangeStartTime = time => {
    this.setState({ time_filter: true });
    let m = moment(time);
    let startTimeFormatted = m.format("HH:mm:ss");
    console.log("startTimeFormatted: " + startTimeFormatted);
    this.setState({ start_time_value: startTimeFormatted });
  };

  onChangeEndTime = time => {
    this.setState({ time_filter: true });
    var m = moment(time);
    let endTimeFormatted = m.format("HH:mm:ss");
    console.log("endTimeFormatted: " + endTimeFormatted);
    this.setState({ end_time_value: endTimeFormatted });
  };

  dateFilteration = info => {
    let result = info.filter(item => {
      return new Date(item.date) >= startDate && new Date(item.date) <= endDate;
    });

    // This if condition is for resetting the original viewed data after clearing date filter.
    if (
      (startDate.toString() && endDate.toString()) === "Invalid Date" ||
      null ||
      undefined
    ) {
      result = this.state.items;
    }
    return result;
  };

  timeFilteration = info => {
    let start = this.state.start_time_value;
    console.log("startTime[TimeFilteration]: ", start);
    let end = this.state.end_time_value;
    console.log("endTime[TimeFilteration]: ", end);

    let result = info.filter(item => {
      return item.time >= start && item.time <= end;
    });

    console.log("result: ", result);
    return result;
  };

  render() {
    let dataFormat = [],
      dateStatus = this.state.date_filter,
      timeStatus = this.state.time_filter;
    dataFormat = this.state.items;

    const columns = [
      {
        title: "Date",
        dataIndex: "date",
        filterDropdown: ({}) => (
          <React.Fragment>
            <RangePicker onChange={this.onChangeDate} />
            <Button /* onClick={this.dateFilteration(dataFormat)}*/>Ok</Button>
          </React.Fragment>
        )
      },
      {
        title: "Time",
        dataIndex: "time",
        filterDropdown: () => (
          <React.Fragment>
            <TimePicker
              // value={this.state.start_time_value}
              onChange={this.onChangeStartTime}
              placeholder="Start Time"
            />
            &nbsp;
            <TimePicker
              // value={this.state.end_value}
              onChange={this.onChangeEndTime}
              placeholder="End Time"
            />
            <Button /* onClick={this.timeFilteration(dataFormat)}*/>Ok</Button>
          </React.Fragment>
        )
      },
      {
        title: "Device ID",
        dataIndex: "device_id",
        key: "device_id",
        filterDropdown: ({
          setSelectedKeys,
          selectedKeys,
          confirm,
          clearFilters
        }) => (
          <div className="custom-filter-dropdown">
            <Input
              ref={ele => (this.searchInput = ele)}
              placeholder="Search..."
              value={selectedKeys[0]}
              onChange={e =>
                setSelectedKeys(e.target.value ? [e.target.value] : [])
              }
              // onPressEnter={this.handleSearch(selectedKeys, confirm)}
            />
            <Button
              type="primary"
              onClick={this.handleSearch(selectedKeys, confirm)}
            >
              Search
            </Button>
            <Button onClick={this.handleReset(clearFilters)}>Reset</Button>
          </div>
        ),
        filterIcon: filtered => (
          <Icon
            type="smile-o"
            style={{ color: filtered ? "#108ee9" : "#aaa" }}
          />
        ),
        onFilter: (value, record) =>
          record.device_id.toLowerCase().includes(value.toLowerCase()),
        onFilterDropdownVisibleChange: visible => {
          if (visible) {
            setTimeout(() => {
              this.searchInput.focus();
            });
          }
        },
        render: text => {
          console.log("text: ", text);
          const { searchText } = this.state;
          console.log("searchText: ", searchText);
          return searchText.toString() ? (
            <span>
              {text.split(new RegExp(`(${searchText.toString()})`, "gi")).map(
                (fragment, i) =>
                  fragment.toLowerCase() === searchText.toLowerCase() ? (
                    <span key={i} className="highlight">
                      {fragment}
                    </span>
                  ) : (
                    fragment
                  ) // eslint-disable-line
              )}
            </span>
          ) : (
            text
          );
        }
      },
      {
        title: "Campaign Name",
        dataIndex: "campaign_name",
        filterDropdown: ({
          setSelectedKeys,
          selectedKeys,
          confirm,
          clearFilters
        }) => (
          // Search Filter as component....
          // <SearchFilter onSearch={this.onSearch} />
          <div className="custom-filter-dropdown">
            <Input
              ref={ele => (this.searchInput = ele)}
              placeholder="Search name"
              value={selectedKeys[0]}
              onChange={e =>
                setSelectedKeys(e.target.value ? [e.target.value] : [])
              }
              onPressEnter={this.handleSearch(selectedKeys, confirm)}
            />
            <Button
              type="primary"
              onClick={this.handleSearch(selectedKeys, confirm)}
            >
              Search
            </Button>
            <Button onClick={this.handleReset(clearFilters)}>Reset</Button>
          </div>
        ),
        filterIcon: filtered => (
          <Icon
            type="smile-o"
            style={{ color: filtered ? "#108ee9" : "#aaa" }}
          />
        ),
        onFilter: (value, record) =>
          record.campaign_name.toLowerCase().includes(value.toLowerCase()),
        onFilterDropdownVisibleChange: visible => {
          if (visible) {
            setTimeout(() => {
              console.log("searchInput.focus()");
              this.searchInput.focus();
            });
          }
        },
        render: text => {
          const { searchText } = this.state;
          return searchText ? (
            <span>
              {text
                .split(new RegExp(`(?<=${searchText})|(?=${searchText})`, "i"))
                .map(
                  (fragment, i) =>
                    fragment.toLowerCase() === searchText.toLowerCase() ? (
                      <span key={i} className="highlight">
                        {fragment}
                      </span>
                    ) : (
                      fragment
                    ) // eslint-disable-line
                )}
            </span>
          ) : (
            text
          );
        }
      },
      {
        title: "Container Type",
        dataIndex: "container_type",
        filterDropdown: ({
          setSelectedKeys,
          selectedKeys,
          confirm,
          clearFilters
        }) => (
          <div className="custom-filter-dropdown">
            <Input
              ref={ele => (this.searchInput = ele)}
              placeholder="Search name"
              value={selectedKeys[0]}
              onChange={e =>
                setSelectedKeys(e.target.value ? [e.target.value] : [])
              }
              onPressEnter={this.handleSearch(selectedKeys, confirm)}
            />
            <Button
              type="primary"
              onClick={this.handleSearch(selectedKeys, confirm)}
            >
              Search
            </Button>
            <Button onClick={this.handleReset(clearFilters)}>Reset</Button>
          </div>
        ),
        filterIcon: filtered => (
          <Icon
            type="smile-o"
            style={{ color: filtered ? "#108ee9" : "#aaa" }}
          />
        ),
        onFilter: (value, record) =>
          record.container_type.toLowerCase().includes(value.toLowerCase()),
        onFilterDropdownVisibleChange: visible => {
          if (visible) {
            setTimeout(() => {
              this.searchInput.focus();
            });
          }
        },
        render: text => {
          const { searchText } = this.state;
          return searchText ? (
            <span>
              {text
                .split(new RegExp(`(?<=${searchText})|(?=${searchText})`, "i"))
                .map(
                  (fragment, i) =>
                    fragment.toLowerCase() === searchText.toLowerCase() ? (
                      <span key={i} className="highlight">
                        {fragment}
                      </span>
                    ) : (
                      fragment
                    ) // eslint-disable-line
                )}
            </span>
          ) : (
            text
          );
        }
      },
      /*
        {
          title: "Created on Server",
          dataIndex: "created_server"
        },*/
      {
        title: "Advertiser Name",
        dataIndex: "advertiser_name",
        filterDropdown: ({
          setSelectedKeys,
          selectedKeys,
          confirm,
          clearFilters
        }) => (
          <div className="custom-filter-dropdown">
            <Input
              ref={ele => (this.searchInput = ele)}
              placeholder="Search name"
              value={selectedKeys[0]}
              onChange={e =>
                setSelectedKeys(e.target.value ? [e.target.value] : [])
              }
              onPressEnter={this.handleSearch(selectedKeys, confirm)}
            />
            <Button
              type="primary"
              onClick={this.handleSearch(selectedKeys, confirm)}
            >
              Search
            </Button>
            <Button onClick={this.handleReset(clearFilters)}>Reset</Button>
          </div>
        ),
        filterIcon: filtered => (
          <Icon
            type="smile-o"
            style={{ color: filtered ? "#108ee9" : "#aaa" }}
          />
        ),
        onFilter: (value, record) =>
          record.advertiser_name.toLowerCase().includes(value.toLowerCase()),
        onFilterDropdownVisibleChange: visible => {
          if (visible) {
            setTimeout(() => {
              this.searchInput.focus();
            });
          }
        },
        render: text => {
          const { searchText } = this.state;
          return searchText ? (
            <span>
              {text
                .split(new RegExp(`(?<=${searchText})|(?=${searchText})`, "i"))
                .map(
                  (fragment, i) =>
                    fragment.toLowerCase() === searchText.toLowerCase() ? (
                      <span key={i} className="highlight">
                        {fragment}
                      </span>
                    ) : (
                      fragment
                    ) // eslint-disable-line
                )}
            </span>
          ) : (
            text
          );
        }
      }
    ];

    const data = dateStatus ? this.dateFilteration(dataFormat) : dataFormat;
    const filtredData = timeStatus ? this.timeFilteration(data) : data;
    lastData = filtredData;

    return (
      <div>
        <AdvertiserNameChart advertisersData={lastData} />
        <ContainerTypeChart containerTypeData={lastData} />
        <Summary
          noOfAllLogs={this.state.no_of_items}
          pageNumber={this.state.page_number}
        />
        <Table
          columns={columns}
          dataSource={lastData}
          rowKey={data.dataIndex}
          onChange={this.onChange}
        />
      </div>
    );
  }
}

export default DataTable;
