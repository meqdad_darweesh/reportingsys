import React, { Component } from "react";
import { Menu, Icon } from "antd";

class TableMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  selectItem = (item, key, keyPath) => {
    if (key === "1") {
      console.log("key: ", key);
    }
  };

  render() {
    return (
      <div style={{ width: 195 }}>
        <br />
        <br />
        <br />
        <Menu
          defaultSelectedKeys={["1"]}
          defaultOpenKeys={["sub1"]}
          mode="inline"
          theme="dark"
          onClick={this.selectItem}
        >
          <Menu.Item key="1">
            <Icon type="table" />
            <span>Table</span>
          </Menu.Item>
        </Menu>
      </div>
    );
  }
}

export default TableMenu;
