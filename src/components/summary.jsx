import React, { Component } from "react";
import { Badge } from "antd";

class Summary extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items_no: this.props.noOfAllLogs,
      page_number: this.props.pageNumber
    };
  }

  //WARNING! To be deprecated in React v17. Use new lifecycle static getDerivedStateFromProps instead.
  componentWillReceiveProps(nextProps) {
    if (this.props !== nextProps) {
      this.setState({
        page_number: nextProps.pageNumber
      });
    }
  }
  render() {
    return (
      <div style={{ textAlign: "left", height: "28px", marginTop: "10px" }}>
        &nbsp;&nbsp;
        <strong>All logs:&nbsp; </strong>
        <Badge
          count={this.state.items_no}
          style={{
            backgroundColor: "#fff",
            color: "#999",
            boxShadow: "0 0 0 1px #d9d9d9 inset"
          }}
        />
        &nbsp;
        <span style={{ color: "gray" }}>(10 per page)</span>
        &nbsp; &nbsp;
        <strong>Page #: &nbsp;</strong>
        <Badge
          count={this.state.page_number}
          style={{
            backgroundColor: "#fff",
            color: "#999",
            boxShadow: "0 0 0 1px #d9d9d9 inset"
          }}
        />
      </div>
    );
  }
}

export default Summary;
