import React, { Component } from "react";
import { Form, Icon, Input, Button, message } from "antd";
import PropTypes from "prop-types";

const FormItem = Form.Item;
let username, password;

class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        username: "",
        password: ""
      }
    };
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        // this.setState({
        //   data.username: username,
        //   data.password: password
        // });

        // console.log("Received values password of form: ", values.password);
        // console.log("Received values username of form: ", values.userName);
        if (values.userName === "123" && values.password === "321") {
          message.success("Confirmed.");
          username = values.userName;
          password = values.password;
          let credentials = {
            user: username,
            pass: password
          };
          this.props.submit(credentials);
        } else {
          message.error("Error, try again.");
        }
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    // const WrappedNormalLoginForm = Form.create({})(Login);
    // const { data } = this.state;
    return (
      <div>
        <Form onSubmit={this.handleSubmit} className="login-form">
          <FormItem>
            {getFieldDecorator("userName", {
              rules: [
                { required: true, message: "Please input your username!" }
              ]
            })(
              <Input
                // setFieldsValue={username}
                prefix={
                  <Icon type="user" style={{ color: "rgba(0,0,0,.25)" }} />
                }
                placeholder="Username"
              />
            )}
          </FormItem>
          <FormItem>
            {getFieldDecorator("password", {
              rules: [
                { required: true, message: "Please input your Password!" }
              ]
            })(
              <Input
                // setFieldsValue={password}
                prefix={
                  <Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />
                }
                type="password"
                placeholder="Password"
              />
            )}
          </FormItem>
          <FormItem>
            <Button
              type="primary"
              htmlType="submit"
              className="login-form-button"
            >
              Log in
            </Button>
            &nbsp; &nbsp; Or &nbsp;
            <a href="">register now!</a>
          </FormItem>
        </Form>
      </div>
    );
  }
}

LoginForm.PropTypes = {
  submit: PropTypes.func.isRequired
};

export default Form.create()(LoginForm);
