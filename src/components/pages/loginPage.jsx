import React, { Component } from "react";
import LoginForm from "../forms/loginForm";
import { Link } from "react-router-dom";
import { Card } from "antd";
class LoginPage extends Component {
  submit = credentials => {
    console.log("credentials...", credentials);
    this.props.history.push("/dashboard");
  };
  render() {
    return (
      <div style={{ marginLeft: 380, marginTop: 150 }}>
        <Card
          title="Login"
          //extra={<a href="#">More</a>}
          style={{ width: 600 }}
        >
          <LoginForm submit={this.submit} />
        </Card>
      </div>
    );
  }
}

export default LoginPage;
