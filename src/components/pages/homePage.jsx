import React, { Component } from "react";
import { Link } from "react-router-dom";

class HomePage extends Component {
  render() {
    return (
      <div style={{ margin: 100 }}>
        <h2>Jwar Report System</h2>
        <Link to="/loginPage">Login</Link>
      </div>
    );
  }
}

export default HomePage;
